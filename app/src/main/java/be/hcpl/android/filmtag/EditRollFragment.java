package be.hcpl.android.filmtag;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import be.hcpl.android.filmtag.model.Roll;
import be.hcpl.android.filmtag.template.TemplateFragment;
import be.hcpl.android.filmtag.util.StorageUtil;
import butterknife.Bind;

/**
 * Created by hcpl on 1/08/15.
 */
public class EditRollFragment extends TemplateFragment {

    private static final String KEY_EDIT_ROLL = "edit_roll";

    @Bind(R.id.edit_type)
    AutoCompleteTextView editType;

    @Bind(R.id.edit_exposed)
    EditText editSpeed;
    @Bind(R.id.edit_frames)
    EditText editFrames;
    @Bind(R.id.edit_notes)
    EditText editNotes;
    @Bind(R.id.edit_tags)
    EditText editTags;

    @Bind(R.id.check_developed)
    CheckBox developed;

    private Roll roll;

    /**
     * use for creating new rolls
     *
     * @return
     */
    public static EditRollFragment newInstance() {
        Bundle args = new Bundle();
        EditRollFragment fragment = new EditRollFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * use for editing existing rolls
     *
     * @return
     */
    public static EditRollFragment newInstance(Roll roll) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_EDIT_ROLL, roll);
        EditRollFragment fragment = new EditRollFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_form_roll;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_EDIT_ROLL, roll);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            roll = (Roll) savedInstanceState.getSerializable(KEY_EDIT_ROLL);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if (args != null) {
            roll = (Roll) args.getSerializable(KEY_EDIT_ROLL);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_film, menu);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // prefill data if possible
        if (roll != null) {
            editType.setText(roll.getType());
            editNotes.setText(roll.getNotes());
            if (roll.getSpeed() != 0)
                editSpeed.setText(String.valueOf(roll.getSpeed()));
            if (roll.getFrames() != 0)
                editFrames.setText(String.valueOf(roll.getFrames()));
            developed.setChecked(roll.isDeveloped());
            // populate the tags here
            if (roll.getTags() != null && !roll.getTags().isEmpty())
                editTags.setText(TextUtils.join(" ", roll.getTags()));
        }
        // populate with defaults here
        else {
            // have preferences for this
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            editSpeed.setText(prefs.getString("key_default_iso", String.valueOf(200)));
            editFrames.setText(prefs.getString("key_default_frames", String.valueOf(36)));
        }

        // autocomplete
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, getTypeSuggestions());
        editType.setAdapter(adapter);
    }

    private String[] getTypeSuggestions() {
        List<Roll> rolls = StorageUtil.getAllRolls(((MainActivity) getActivity()));
        if (rolls == null)
            return new String[]{};
        String[] existingTypes = new String[rolls.size()];
        for (int i = 0; i < rolls.size(); i++) {
            existingTypes[i] = rolls.get(i).getType();
        }
        return existingTypes;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create:
                createNewItem();
                return true;
            case android.R.id.home:
                backToOverview();
                return true;
        }
        return false;
    }

    private void backToOverview() {
        // popping first is one option, going back in stack is better
        MainActivity activity = ((MainActivity) getActivity());
        activity.getSupportFragmentManager().popBackStackImmediate();
        activity.getSupportFragmentManager().popBackStackImmediate();
        if (roll == null)
            activity.switchContent(FilmRollListFragment.newInstance());
        else {
            activity.switchContent(FilmFrameListFragment.newInstance(roll));
        }
    }

    private void createNewItem() {
        boolean newRoll = false;
        // insert the new item
        if (roll == null) {
            roll = new Roll();
            newRoll = true;
        }
        roll.setType(editType.getText().toString());
        roll.setNotes(editNotes.getText().toString());
        roll.setDeveloped(developed.isChecked());
        roll.setTags(Arrays.asList(TextUtils.split(editTags.getText().toString(), " ")));
        try {
            roll.setSpeed(Integer.parseInt(editSpeed.getText().toString()));
        } catch (NumberFormatException nfe) {
            Toast.makeText(getActivity(), R.string.err_parsing_failed, Toast.LENGTH_SHORT).show();
        }
        try {
            roll.setFrames(Integer.parseInt(editFrames.getText().toString()));
        } catch (NumberFormatException nfe) {
            Toast.makeText(getActivity(), R.string.err_parsing_failed, Toast.LENGTH_SHORT).show();
        }

        // store new roll
        if (newRoll)
            StorageUtil.addNewRoll((MainActivity) getActivity(), roll);
        else
            StorageUtil.updateRoll((MainActivity) getActivity(), roll);


        // navigate to overview
        backToOverview();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setHomeAsUp(true);
    }

}
