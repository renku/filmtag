package be.hcpl.android.filmtag.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hcpl on 1/08/15.
 */
public class Roll implements Serializable{

    /**
     * by adding an ID we can delete items by ID and change them
     */
    private long id;

    /**
     * type indication of film roll, think of brand
     */
    private String type;

    /**
     * ISO speed of film roll
     */
    private int speed = 200;

    /**
     * number of frames of film roll, note that this is used to get the number of frames available
     * for recording settings
     */
    private int frames = 36;

    /**
     * notes for this film roll
     */
    private String notes;

    /**
     * if film was developed or not
     */
    private boolean developed;

    /**
     * a collection of tags for this item
     */
    private List<String> tags = new ArrayList<>();

    public Roll() {
        setId(System.currentTimeMillis()); // generates unique ID for all objects created
    }

    public Roll(String type, int speed, int frames) {
        this();
        this.type = type;
        this.speed = speed;
        this.frames = frames;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getFrames() {
        return frames;
    }

    public void setFrames(int frames) {
        this.frames = frames;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isDeveloped() {
        return developed;
    }

    public void setDeveloped(boolean developed) {
        this.developed = developed;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return new StringBuilder(type).append(" @ ISO ").append(speed).append(" # ").append(String.valueOf(frames)).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Roll)) return false;

        Roll roll = (Roll) o;

        return getId() == roll.getId();

    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
